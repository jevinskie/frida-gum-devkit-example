#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, const char **argv) {
	char buf[4096];
	int fd = open(argv[1], O_RDONLY);
	read(fd, buf, sizeof(buf));
	printf("%s: %s\n", argv[1], buf);
	int ret = close(fd);
	printf("close ret: 0x%08x\n", ret);
	return 0;
}
