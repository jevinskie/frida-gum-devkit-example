TARGETS = frida-gum-example.so hello hello-static-injection

all: $(TARGETS)

.PHONY: clean run-dynamic run-static

frida-gum-example.so: frida-gum-example.c
	$(CC) -o $@ -shared  $^ -fPIC libfrida-gum.a -lpthread -ldl -ffunction-sections -fdata-sections -Wl,--gc-sections

hello: hello.c
	$(CC) -o $@ $^

hello-static-injection: hello frida-gum-example.so
	cp hello $@
	patchelf --add-needed ./frida-gum-example.so $@

clean:
	rm -f $(TARGETS)

run-dynamic: hello frida-gum-example.so hello.c
	LD_PRELOAD=$(PWD)/frida-gum-example.so ./hello hello.c

run-static: hello-static-injection frida-gum-example.so
	./hello-static-injection hello.c
